const btn = document.querySelector('.btn');
btn.addEventListener('click', findIP);

async function findIP() {
  const { ip } = await fetch('https://api.ipify.org/?format=json')
    .then(resp => resp.json());
  const { timezone, country, regionName, city, zip } = await fetch(`http://ip-api.com/json/${ip}`)
    .then(resp => resp.json());
  const continent = timezone.split("/")[0];
  btn.insertAdjacentHTML(
    'afterend',
    `<p>Континент: ${continent},</p>
    <p>Країна: ${country},</p>
    <p>Регіон: ${regionName},</p>
    <p>Місто: ${city},</p>
    <p>Індекс: ${zip}.</p>`
  );
}